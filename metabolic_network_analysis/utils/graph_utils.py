import networkx as nx


def get_compounds(g, nodes: set | list) -> set:
    # filters out reactions from list of nodes
    return {n for n in nodes if n in [v for v, attr in g.nodes(data=True) if not attr["is_reaction"]]}


def get_reactions(g, nodes: set | list) -> set:
    # filters out compounds from list of nodes
    return {n for n in nodes if g.nodes[n]["is_reaction"]}


def get_reactions_as_list(g, nodes: set | list) -> list:
    # filters out compounds from list of nodes
    return [n for n in nodes if g.nodes[n]["is_reaction"]]

#takes graph and reaction, returns all nodes connecteded to incoming edges of reaction nodes i.e. the edcuts
def get_educts(g, reaction: str | list | set) -> set:
    # returns the educts of a given reaction
    if isinstance(reaction, str):
        return {e[0] for e in g.in_edges(reaction)}
    elif isinstance(reaction, list) or isinstance(reaction, set):
        return {e for r in reaction for e in get_educts(g, r)}
    else:
        raise NotImplementedError


#takes graph and reaction, returns all nodes connected to outgoing edges of reaction nodes i.e. the products
def get_products(g, reaction: str | list | set) -> set:
    # returns the products of a given reaction
    if isinstance(reaction, str):
        return {e[1] for e in g.out_edges(reaction)}
    elif isinstance(reaction, list) or isinstance(reaction, set):
        return {p for r in reaction for p in get_products(g, r)}
    else:
        raise NotImplementedError


#create graph by iterating through all reactions. split in educt, product, reaction type and if reversible.
#ads reaction node followed by educts and products. adds edges from educt to reaction node and from reaction node to product
#if reaction is reversible a reverse reaction node is added and connected to products and educts
#products of last reaction will be edcuts of next reaction, thus the graph is connected
def create_graph(reactions: list, use_undirected=False) -> nx.Graph:
    g = nx.DiGraph()

    for r in reactions:
        educts = r["educts"]
        products = r["products"]
        reaction = r["meta_net_x_id"]
        reversible = r["reversible"]

        # generate name and reversed smiles for reverse reaction
        reverse_reaction = reaction + "_reversed"
        smiles = r["smiles"]
        smiles_split = smiles.split(">>")
        smiles_reversed = smiles_split[1] + ">>" + smiles_split[0]

        g.add_node(reaction, color="red", reversible=reversible, smiles=smiles, is_reaction=True)
        if reversible:
            # addional reaction for reversed reaction
            g.add_node(reverse_reaction, color="red", reversible=reversible, smiles=smiles_reversed, is_reaction=True)

        for e in educts:
            num_e = len([x for x in educts if x == e])
            g.add_node(e, color="green", is_reaction=False)
            g.add_edge(e, reaction, weight=num_e)
            if reversible:
                # add edge to reverse reaction
                g.add_edge(reverse_reaction, e, weight=num_e)

        for p in products:
            num_p = len([x for x in products if x == p])
            g.add_node(p, color="green", is_reaction=False)
            g.add_edge(reaction, p, weight=num_p)
            if reversible:
                # add edge to reverse reaction
                g.add_edge(p, reverse_reaction, weight=num_p)

    if use_undirected:
        return g.to_directed()
    else:
        return g
