import os

RESOURCES_BASE_PATH = os.path.abspath("./resources")
SMILES_SUFFIX = ".smiles_list"
ATOM_MAP_SUFFIX = ".mapped_smiles"
ATN_SUFFIX = ".gml"
PROTEOM_SUFFIX = ".faa"


def get_reactions_from_smiles(smiles_file_path: str, omit_loops: bool = True) -> list:
    """
    Parses a smiles file
    :param smiles_file_path:
    :return:
    """

    with open(smiles_file_path, "r") as smiles_file:
        smiles_lines_raw = [line.replace("\n", "") for line in smiles_file.readlines() if line != "\n"]

    def _extract_representation(lines: list, output: list | None = None) -> list:
        if output is None:
            output = list()

        if len(lines) == 0:
            return output

        id_line, _, name_line, smiles_line, *rest = lines

        try:
            id_line_split = id_line.split(" ")
            bigg_id = id_line_split[2]
            meta_net_id = id_line_split[4]
            reversible = id_line_split[6] == "True"
            names_split = name_line.split(" = ")
            educts = names_split[0].split(" + ")
            products = names_split[1].split(" + ")
            smiles = smiles_line
        except:
            return _extract_representation(rest, output)

        # skip loops if omit_loops is set to true
        if omit_loops:
            smiles_split = smiles.split(">>")
            if smiles_split[0] == smiles_split[1]:
                return _extract_representation(rest, output)

        output.append({
            "bigg_id": bigg_id,
            "meta_net_x_id": meta_net_id,
            "reversible": reversible,
            "educts": educts,
            "products": products,
            "smiles": smiles
        })

        return _extract_representation(rest, output)

    reactions = _extract_representation(smiles_lines_raw)
    return reactions


def get_reactions_for_species(species_name: str, cultivation_medium: str) -> list:
    species_info = get_info_for_species(species_name, cultivation_medium)
    return get_reactions_from_smiles(species_info["smiles_file"])


def get_reaction_by_name(reaction_name: str, reaction_list: list) -> list:
    try:
        return [r for r in reaction_list if r["bigg_id"] == reaction_name or r["meta_net_x_id"] == reaction_name][0]
    except IndexError:
        raise KeyError(f"Reaction {reaction_name} does not exist in list")


def get_species_info(base_dir: str, species_dir_name: str) -> dict:
    species_name, cultivation_medium = species_dir_name.split("_")
    species_dir = os.path.join(base_dir, species_dir_name)
    smiles_file = os.path.join(species_dir, f"{species_dir_name}{SMILES_SUFFIX}")
    am_file = os.path.join(species_dir, f"{species_dir_name}{ATOM_MAP_SUFFIX}")
    atn_file = os.path.join(species_dir, f"{species_dir_name}{ATN_SUFFIX}")
    try:
        proteom_file = os.path.join(species_dir, f"{species_dir_name}{PROTEOM_SUFFIX}")
    except:
        proteom_file = None

    return {
        "name": species_name,
        "cultivation_medium": cultivation_medium,
        "dir": species_dir,
        "smiles_file": smiles_file,
        "atm_file": am_file,
        "atn_file": atn_file,
        "proteom_file": proteom_file
    }


def get_all_species_info(species_dir: str = RESOURCES_BASE_PATH):
    species_dir_names = [x for x in os.listdir(RESOURCES_BASE_PATH) if
                         os.path.isdir(os.path.join(RESOURCES_BASE_PATH, x))]
    species_info = [get_species_info(species_dir, s) for s in species_dir_names]
    return species_info


def get_info_for_species(species_name: str, cultivation_medium: str = None, species_info: list = None) -> dict:
    if species_info is None:
        species_info = get_all_species_info()
    if cultivation_medium is None:
        index = [s["name"] for s in species_info].index(species_name)
    else:
        index = [s["name"] + s["cultivation_medium"] for s in species_info].index(species_name + cultivation_medium)
    return species_info[index]
