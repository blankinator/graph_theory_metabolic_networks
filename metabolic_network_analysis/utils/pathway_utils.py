from utils import graph_utils


#checks if reaction nodes are reachable. Get all Educts (depending on as_output bool) from given node and checks if they are part of the compound list.
#if yes, they are become part of the compound list
def get_reachable_reactions(g, given_inputs: set, as_outputs: bool = False) -> (set, set):
    reaction_nodes = graph_utils.get_reactions(g, g.nodes)
    current_compounds = given_inputs
    last_compounds = set()
    reachable_reactions = set()

    while last_compounds != current_compounds:
        def is_reachable(g, reaction: str, given_inputs: set):
            if as_outputs:
                compounds = graph_utils.get_products(g, reaction)
                is_contained = [c in given_inputs for c in compounds]
                return any(is_contained)
            else:
                compounds = graph_utils.get_educts(g, reaction)
                is_contained = [c in given_inputs for c in compounds]
                return all(is_contained)
#add reaction node if is_reachable is true (educts or products are in compound list)
        reachable_reactions = reachable_reactions.union(
            {r for r in reaction_nodes if is_reachable(g, r, current_compounds)})
#for all edges of reachable reactions either add educt or product
        if as_outputs:
            out_compounds = {e[0] for r in reachable_reactions for e in g.in_edges(r)}
        else:
            out_compounds = {e[1] for r in reachable_reactions for e in g.out_edges(r)}

        last_compounds = current_compounds
        current_compounds = current_compounds.union(out_compounds)

    return reachable_reactions, current_compounds


def get_pathway_nodes(g, given_inputs: set, desired_outputs: set) -> (set, set):
    reactions_by_input, products_by_input = get_reachable_reactions(g, given_inputs)
    reactions_by_output, educts_by_output = get_reachable_reactions(g, desired_outputs, as_outputs=True)
    reactions = reactions_by_input.intersection(reactions_by_output)
    educts = {e[0] for r in reactions for e in g.in_edges(r)}
    products = {e[1] for r in reactions for e in g.out_edges(r) if e[1] in desired_outputs}
    compounds = educts.union(products)
    return reactions, compounds


def get_pathway_subgraph(g, given_inputs, desired_outputs):
    reactions, compounds = get_pathway_nodes(g, given_inputs, desired_outputs)
    sub = g.subgraph(reactions.union(compounds))
    return sub
