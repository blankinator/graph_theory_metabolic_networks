energy_compounds = {"AMP", "ATP", "ADP", "GDP", "NAD(+)", "NADH", "NADP(+)", "NADPH", "FAD", "FADH2", "UTP", "CTP",
                    "heme b", "CoA", "FMN", "hydrogen sulfide"}
central_compounds = {
    "H2O",
    "NH4(+)",
    "phosphate",
    "CO2"
}
# start_compounds = {"D-glucose 6-phosphate"}
start_compounds = {"D-glucose"}

amino_acids = {
    "L-arginine",
    "L-valine",
    "L-methionine",
    "L-glutamate",
    "L-glutamine",
    "L-tyrosine",
    "L-tryptophan",
    "L-proline",
    "L-cysteine",
    "L-histidine",
    "L-asparagine",
    "L-aspartate",
    "L-phenylalanine",
    "L-threonine",
    "L-lysine",
    "L-serine",
    "L-isoleucine",
    "L-leucine",
    "glycine",
    "L-alanine",
}
print(len(amino_acids))

given_inputs = energy_compounds.union(central_compounds).union(start_compounds)

representation_to_amino_mapping = {
    "R": "L-arginine",
    "V": "L-valine",
    "M": "L-methionine",
    "E": "L-glutamate",
    "Q": "L-glutamine",
    "Y": "L-tyrosine",
    "W": "L-tryptophan",
    "P": "L-proline",
    "C": "L-cysteine",
    "H": "L-histidine",
    "N": "L-asparagine",
    "D": "L-aspartate",
    "F": "L-phenylalanine",
    "T": "L-threonine",
    "K": "L-lysine",
    "S": "L-serine",
    "I": "L-isoleucine",
    "G": "glycine",
    "A": "L-alanine",
    "L": "L-Leucine",
    "L": "L-leucine"
}

amino_to_representation_mapping = {v: k for k, v in representation_to_amino_mapping.items()}


def get_mapping(input_name: str) -> str:
    # maps a given input to either the respective amino acid name or its representational character
    if input_name in amino_acids:
        return amino_to_representation_mapping[input_name]
    elif input_name in representation_to_amino_mapping.keys():
        return representation_to_amino_mapping[input_name]
    else:
        raise KeyError(f"{input_name} is neither an amino acid nor a representational character of one")
